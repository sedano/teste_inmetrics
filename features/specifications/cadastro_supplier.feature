#language: pt
@cadastro_supplier
Funcionalidade: Login de usuario existente

Cenario: Entrar no site phptravels logar e criar novo cadastro no supplier

    Dado que acesse o site phptravels e faça o login
	Dado que entre em accounts suppliers
	E preencha os dados para nova conta
	Entao valide o cadastro