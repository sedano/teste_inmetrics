Dado("que acesse o site phptravels e faça o login") do
   visit 'https://www.phptravels.net/admin'
   fill_in "email", :with => "admin@phptravels.com" 
   fill_in "password", :with => "demoadmin"  
  find('.ladda-label').click 
  
end


Dado("que entre em accounts suppliers") do
   #find(:css,('href["#ACCOUNTS"]')
   visit 'https://www.phptravels.net/admin/accounts/suppliers/'
  
end

E("preencha os dados para nova conta") do
   find('a.btn-success').click
   fill_in "First name", :with => "teste" 
   fill_in "Last name", :with => "will"  
   fill_in "email", :with => "testewill@teste.com" 
   fill_in "password", :with => "testewill" 
   fill_in "Mobile Number", :with => "11980990001"
   fill_in "address1", :with => "teste endereco" 
   find('.btn-primary').click
   sleep 3
  
  end

Entao("valide o cadastro") do
   visit 'https://www.phptravels.net/admin'
   fill_in "email", :with => "testewill@teste.com" 
   fill_in "password", :with => "testewill"  
  find('.ladda-label').click 

 end 